#!/usr/bin/python3

# Basic details
AUTHOR = "Debian XMPP Team"
SITENAME = "Bits from the Debian XMPP Team"
SITESUBTITLE = "Blog of the Debian XMPP Team"
SITEURL = "https://xmpp-team.pages.debian.net/blog"

# Configuration
TIMEZONE = "UTC"
DEFAULT_LANG = "en"
DELETE_OUTPUT_DIRECTORY = True
THEME = "theme-bits"
DEFAULT_PAGINATION = 5
DISPLAY_PAGES_ON_MENU = True
SUMMARY_MAX_LENGTH = None
LOCALE = "C"

# URL settings
# We might want this for publication
RELATIVE_URLS = True
ARTICLE_URL = "{date:%Y}/{date:%m}/{slug}.html"
ARTICLE_SAVE_AS = "{date:%Y}/{date:%m}/{slug}.html"
ARTICLE_LANG_URL = "{date:%Y}/{date:%m}/{slug}-{lang}.html"
ARTICLE_LANG_SAVE_AS = "{date:%Y}/{date:%m}/{slug}-{lang}.html"

# Feeds settings
FEED_ATOM = "feeds/atom.xml"
FEED_RSS = "feeds/feed.rss"
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = None
TAG_FEED_ATOM = None
TAG_FEED_RSS = None
TRANSLATION_FEED = None

# Do not create category pages
CATEGORIES_SAVE_AS = None
CATEGORY_SAVE_AS = ""
CATEFORY_URL = None

MENUITEMS = (("Home", "/blog/"),)

DEBIAN = (
    ("Debian Project News", "https://www.debian.org/News/"),
    ("Bits from Debian", "https://bits.debian.org/"),
    ("Debian micronews", "https://micronews.debian.org/"),
    ("Planet Debian", "https://planet.debian.org/"),
)

XMPP = (
    ("XMPP Main Page", "https://xmpp.org/"),
    ("Planet Jabber", "https://planet.jabber.org/"),
    ("Debian XMPP Team tracker", "https://tracker.debian.org/teams/xmpp-team/"),
    ("Debian XMPP wiki page", "https://wiki.debian.org/XMPP/"),
)

PATH = "content"
STATIC_PATHS = ["extras/favicon.ico", "images"]
EXTRA_PATH_METADATA = {"extras/favicon.ico": {"path": "favicon.ico"}}

# Plugins

PLUGINS = []
PLUGIN_PATHS = ["plugins"]
