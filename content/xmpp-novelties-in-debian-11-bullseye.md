Title: XMPP Novelties in Debian 11 Bullseye
Slug: xmpp-novelties-in-debian-11-bullseye
Date: 2021-07-13
Tags: xmpp, jabber, debian, bullseye, newinbullseye, ox, yearoftheox

This is not only the Year of the Ox, but also the year of Debian 11, code-named
bullseye. The release lies ahead, [full freeze starts this week][full-freeze].
A good opportunity to take a look at what is new in bullseye.
In this post new programs and new software versions related to
[XMPP, also known as Jabber][xmpp] are presented.
XMPP exists since 1999, and has a diverse and active developers community. It is a
universal communication protocol, used for instant messaging, IoT, WebRTC, and social
applications.
You probably will encounter some oxen in this post.

- [biboumi], XMPP gateway to connect to IRC servers: 8.3 → 9.0  
  The biggest change for users is SASL support: A new field in the Configure ad-hoc
  command lets you set a password that will be used to authenticate to the nick service,
  instead of using the cumbersome NickServ method.  
  Many more changes are listed [in the changelog][biboumi-changes].
- [Dino][dino-im], modern XMPP client: 0.0.git20181129 → 0.2.0  
  Dino in Debian 10 was practically a technology preview. In Debian 11 it is already a
  fully usable client, supporting OMEMO encryption, file upload, image preview, message
  correction and many more features in a clean and beautiful user interface.
- [ejabberd], the extensible realtime platform: 18.12.1 → 21.01.  
  Probably the most important improvement for end-users is
  XEP-0215 support to facilitate modern WebRTC-style audio/video calls.
  ejabberd also integrates more nicely with systemd (e.g., the watchdog feature if
  supported, now). Apart from that, a new configuration validator was introduced, which
  brings a more flexible (but mostly backwards-compatible) syntax. Also, error reporting
  in case of misconfiguration should be way more helpful, now. As a new authentication
  backend, JSON Web Tokens (JWT) can be used. In addition to the XMPP and SIP support,
  ejabberd now includes a full-blown MQTT server. A large number of smaller features has
  been added, performance was improved in many ways, and several bugs were fixed.
  See [the long list of changes][ejabberd-changes].
- [Gajim][gajim], a GTK+-based Jabber client: 1.1.2 → 1.3.1  
  The new Debian release brings many improvements. Gajim’s network code has been
  completely rewritten, which leads to faster connections, better recovery from network
  loss, and less network related hiccups. Customizing Gajim is now easier than ever.
  Thanks to the new settings backend and a completely reworked Preferences window, you
  can adapt Gajim to your needs in just a few seconds.  
  Good for newcomers: account creation is now a lot easier with Gajim’s new assistant.
  The new Profile window gives you many options to tell people more about yourself. You
  can now easily crop your own profile picture before updating it.  
  Group chats actions have been reorganized. It’s now easier to send invitations or
  change your nickname for example. Gajim also received support for chat markers, which
  enables you to see how far your contact followed the conversation. But this is by far
  not everything the new release brings. There are many new and helpful features, such
  as pasting images from your clipboard directly into the chat or playing voice messages
  directly from the chat window.  
  Read more about the new Gajim release in Debian 11 [here][gajim-changes].  
  Furthermore, three more Gajim plugins are now in Debian: [gajim-lengthnotifier],
  [gajim-openpgp] for OX 🐂 (XEP-0373: OpenPGP for XMPP) and [gajim-syntaxhighlight].
- NEW [Kaidan][kaidan] Simple and user-friendly Jabber/XMPP client 0.7.0  
  Kaidan is a simple, user-friendly and modern XMPP chat client. The user interface
  makes use of Kirigami and QtQuick, while the back-end of Kaidan is entirely written in
  C++ using Qt and the Qt-based XMPP library QXmpp. Kaidan runs on mobile and desktop
  systems including Linux, Windows, macOS, Android, Plasma Mobile and Ubuntu Touch.
- [mcabber], small Jabber (XMPP) console client: 1.1.0 → 1.1.2  
  A theme for 256 color terminals is now included, the handling of carbon message copies
  has been improved, and various minor issues have been fixed.
- [Poezio][poezio], Console-based XMPP client: 0.12.1 → 0.13.1  
  This new release brings many improvements, such as Message Archive (XEP-0313) support,
  initial support for OMEMO (XEP-0384) through a plugin, HTTP File Upload support,
  Consitent Color Generation (XEP-0392), and plenty of internal changes and bug fixes.
  Not all changes in 0.13 and 0.13.1 can be listed, see the [CHANGELOG][poezio-changes]
  for a more extensive summary.
- [Profanity][profanity], the console based XMPP client: 0.6.0 → 0.10.0  
  We can not list [all changes][profanity-changes] which have been done,
  but here are some highlights.  
  Support of OMEMO Encryption (XEP-0384). Consistent Color Generation (XEP-0392),
  be aware of the changes in the command to standardize the names of commands. A
  clipboard feature has been added. Highlight unread messages with a different
  color in /wins. Keyboard switch to select the next window with unread messages
  with alt + a. Support for Last Message Correction (XEP-0308), Allow UTF-8
  symbols as OMEMO/OTR/PGP indicator char. Add option to open avatars directly
  (XEP-0084). Add option to define a theme at startup and some changes to improve
  themes. Add possibility to easily open URLs. Add experimental OX 🐂 (XEP-0373,
  XEP-0374) support. Add OMEMO media sharing support, ...  
  There is also a [Profanity light][profanity-light] package in Debian now, the
  best option for systems with tight limits on resources.
- [Prosody][prosody], the lightweight extensible XMPP server: 0.11.2 → 0.11.9  
  Upgrading to the latest stable release of Prosody brings a whole load of
  improvements in the stability, usability and performance departments. It especially
  improves the performance of websockets, and PEP performance for users with many
  contacts. It includes interoperability improvements for a range of clients.
- [prosody-modules], community modules and extensions for Prosody: 0.0~hg20190203 → 0.0~hg20210130  
  The ever-growing collection of goodies to plug into Prosody has a number of exciting
  additions, including a suite of modules to handle
  [invite-based account registration][prosody-modules-invites], and others for
  [moderating messages in group chats][prosody-modules-muc-moderation] (e.g. for removal
  of spam/abuse), server-to-server [federation over Tor][prosody-modules-onions] and
  [client authentication using certificates][prosody-modules-auth-ccert]. Many existing
  community modules received updates as well.
- [Psi][psi], Qt-based XMPP client: 1.3 → 1.5  
  The new version contains important bug fixes.
- [salutatoi], multi-frontends, multi-purposes communication tool: 0.7.0a4 → 0.8.0~hg3453  
  This version is now fully running on Python 3, and has full OMEMO support (one2one,
  groups and files). The CLI frontend (jp) has among new commands a "jp file get" one
  which is comparable to wget with OMEMO support. A file sharing component is included,
  with HTTP Upload and Jingle support. For a list of other improvements, please consult
  [the changelog][salutatoi-changes].  
  Note, that the upstream project has been renamed to "Libervia".
- NEW [sms4you], Personal gateway connecting SMS to XMPP or email 0.0.7  
  It runs with a GSM device over [ModemManager][modemmanager] and uses a lightweight
  XMPP server or a single email account to handle communication in both directions.
- NEW [xmppc], XMPP Command Line Client 0.1.0  
  xmppc is a new [command line tool][xmppc-manpage] for XMPP. It supports some
  basic features of XMPP (request your roster, bookmarks, OMEMO Devices and
  fingerprints). You can send messages with both legacy PGP (XEP-0027) and the
  new OX 🐂 (XEP-0373: OpenPGP for XMPP).

That's all for now. Enjoy Debian 11 bullseye and Happy Chatting!

[biboumi]:https://packages.debian.org/bullseye/biboumi
[biboumi-changes]:https://salsa.debian.org/pkg-voip-team/biboumi/-/blob/debian/9.0-2/CHANGELOG.rst
[dino-im]:https://packages.debian.org/bullseye/dino-im
[ejabberd]:https://packages.debian.org/bullseye/ejabberd
[ejabberd-changes]:https://salsa.debian.org/ejabberd-packaging-team/ejabberd/-/blob/debian/21.01-2/CHANGELOG.md
[full-freeze]:https://lists.debian.org/debian-devel-announce/2021/06/msg00000.html
[gajim]:https://packages.debian.org/bullseye/gajim
[gajim-changes]:https://salsa.debian.org/xmpp-team/gajim/-/blob/debian/1.3.1-1/ChangeLog
[gajim-lengthnotifier]:https://packages.debian.org/bullseye/gajim-lengthnotifier
[gajim-openpgp]:https://packages.debian.org/bullseye/gajim-openpgp
[gajim-syntaxhighlight]:https://packages.debian.org/bullseye/gajim-syntaxhighlight
[kaidan]:https://packages.debian.org/bullseye/kaidan
[mcabber]:https://packages.debian.org/bullseye/mcabber
[modemmanager]:https://packages.debian.org/bullseye/modemmanager
[poezio]:https://packages.debian.org/bullseye/poezio
[poezio-changes]:https://salsa.debian.org/xmpp-team/poezio/-/blob/debian/0.13.1-1/CHANGELOG
[profanity]:https://packages.debian.org/bullseye/profanity
[profanity-changes]:https://salsa.debian.org/xmpp-team/profanity/-/blob/debian/0.10.0-1/CHANGELOG
[profanity-light]:https://packages.debian.org/bullseye/profanity-light
[prosody]:https://packages.debian.org/bullseye/prosody
[prosody-modules]:https://packages.debian.org/bullseye/prosody-modules
[prosody-modules-auth-ccert]:https://salsa.debian.org/xmpp-team/prosody-modules/-/blob/debian/0.0_hg20210130.dd3bfe8f182e+dfsg-2/mod_auth_ccert/README.markdown
[prosody-modules-invites]:https://salsa.debian.org/xmpp-team/prosody-modules/-/blob/debian/0.0_hg20210130.dd3bfe8f182e+dfsg-2/mod_invites/README.markdown
[prosody-modules-muc-moderation]:https://salsa.debian.org/xmpp-team/prosody-modules/-/blob/debian/0.0_hg20210130.dd3bfe8f182e+dfsg-2/mod_muc_moderation/README.markdown
[prosody-modules-onions]:https://salsa.debian.org/xmpp-team/prosody-modules/-/blob/debian/0.0_hg20210130.dd3bfe8f182e+dfsg-2/mod_onions/README.markdown
[psi]:https://packages.debian.org/bullseye/psi
[salutatoi]:https://packages.debian.org/source/bullseye/salutatoi
[salutatoi-changes]:https://salsa.debian.org/xmpp-team/salutatoi/-/blob/debian/0.8.0_hg3453.864485605d12-3/CHANGELOG
[sms4you]:https://packages.debian.org/bullseye/sms4you
[xmpp]:https://wiki.debian.org/XMPP/
[xmppc]:https://packages.debian.org/bullseye/xmppc
[xmppc-manpage]:https://manpages.debian.org/bullseye/xmppc/xmppc.1.en.html
