Title: Debian XMPP Team Starts a Blog
Slug: debian-xmpp-team-starts-a-blog
Date: 2019-05-17 23:30
Author: Martin
Tags: debian, xmpp, blog
Status: published

The Debian XMPP Team, the people who package Dino, Gajim, Mcabber,
Movim, Profanity, Prosody, Psi+, Salut à Toi, Taningia, and a couple
of other packages related to XMPP a.k.a. Jabber for Debian, have this
blog now. We will try to post interesting stuff here — when it's ready!
