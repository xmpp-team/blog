Title: New Dino in Debian
Slug: dino-with-jet-and-bookmarks2
Date: 2019-10-13 22:00
Author: Martin
Tags: debian, dino, ejabberd, gajim, newsletter, profanity, xmpp

Dino (`dino-im` in Debian), the modern and beautiful chat client for
the desktop, has some nice, new features. Users of Debian testing
(bullseye) might [like to try them][dino-im]:

* XEP-0391: Jingle Encrypted Transports (explained [here][jet])
* XEP-0402: Bookmarks 2 (explained [here][bm2])

Note, that users of Dino on Debian 10 (buster)
[should upgrade][dino-im:sec] to version `0.0.git20181129-1+deb10u1`,
because of a number of security issues, that have been found
(CVE-2019-16235, CVE-2019-16236, CVE-2019-16237).

There have been other XMPP related updates in Debian since release of
buster, among them:

* [Ejabberd 19.08-2][ejabberd]
* [Gajim 1.1.3-2][gajim] and [Gajim OMEMO 2.6.29-1][gajim-omemo] plugin
* [Profanity 0.7.1][profanity], now with OMEMO encryption support!

You might be interested in the Octobers [XMPP newsletter][newsletter],
also available in [German][newsletter-de].

[bm2]:https://bouah.net/2019/10/sprint-in-the-cold-north/#groupchat-bookmarks-bookmarks-2
[dino-im:sec]:https://packages.debian.org/buster/dino-im
[dino-im]:https://packages.debian.org/bullseye/dino-im
[ejabberd]:https://packages.debian.org/bullseye/ejabberd
[gajim-omemo]:https://packages.debian.org/bullseye/gajim-omemo
[gajim]:https://packages.debian.org/bullseye/gajim
[jet]:https://blog.jabberhead.tk/summer-of-code-2017/#xep-xxxx---jingle-encrypted-transports-jet
[newsletter-de]:https://www.jabber.de/xmpp-newsletter-01-oktober-2019-fosdem-2020-modernisierung-von-xmpp-peer-networks/
[newsletter]:https://XMPP.org/2019/10/newsletter-01-october/
[profanity]:https://packages.debian.org/bullseye/profanity
