Title: XMPP What's new in Debian 12 bookworm
Slug: xmpp-whats-new-in-bookworm
Date: 2023-03-01
Tags: xmpp, jabber, debian, bookworm

On Tue 13 July 2021 there was a
[blog post](https://xmpp-team.pages.debian.net/blog/2021/07/xmpp-novelties-in-debian-11-bullseye.html)
of new XMPP related software releases which have been uploaded to Debian 11 (bullseye).
Today, we will inform you about updates for the upcoming Debian release bookworm.

A lot of new releases have been provided by the upstream projects. There were lot of changes
to the XMPP clients like Dino, Gajim, Profanity, Poezio and others. Also the XMPP servers have been
enhanced.

Unfortunately, we can not provide a list of all the changes which have been done,
but will try to highlight some of the changes and new features.

BTW, feel free to join the Debian User Support on Jabber at
[xmpp:debian@conference.debian.org?join](xmpp:debian@conference.debian.org?join).

You can find a list of 58 packages of the Debian XMPP team on the
[XMPP QA Page](https://qa.debian.org/developer.php?login=pkg-xmpp-devel%40lists.alioth.debian.org).

- [Dino][dino-im], modern XMPP client has been upgraded from
  0.2.0 to 0.4.0. The new version supports encrypted calls and group calls and
  reactions give you a way to respond to a message with an emoji.
  You can find more information about [Dino 0.3.0](https://dino.im/blog/2022/02/dino-0.3-release/) and
  [Dino 0.4.0](https://dino.im/blog/2023/02/dino-0.4-release/) in
  the release notes of the upstream project. Dino is using GTK4 /
  libadwaita which provides widgets for mobile-friendly UIs.
  Changes has been done on the main view of Dino.
- [Gajim][gajim], a GTK+-based Jabber client has been upgraded from 1.3.1
  to 1.7.1. Since 1.4 Gajim has got a new UI, which supports
  spaces. 1.5.2 supports a content viewer for PEP nodes. 1.6.0 is
  using libsoup3 and python 3.10. Audio preview looks a lot nicer
  with a wave graph visualization and profile images (avatar) are
  not limited to only JPG anymore. The plugins
  gajim-appindicatorintegration, gajim-plugininstaller,
  gajim-syntaxhighlight und gajim-urlimagepreview are obsolete,
  these features has been moved to gajim.
  There were a lot of releases in Gajim. You can find the full
  story at https://gajim.org/post/
- [Profanity][profanity], the console based XMPP client has been upgraded
  from 0.10.0 to 0.13.1. Profanity supports XEP-0377 Spam
  Reporting, and XEP-0157 server contact information discovery.
  It now marks a window with an attention flag, updated HTTP Upload
  XEP-0363, and messages can be composed with an external editor. It also features easy quoting,
  in-band account registration (XEP-0077), Print OMEMO verification
  QR code, and many more.
- [Kaidan][kaidan], a simple and user-friendly Jabber/XMPP client based on Qt has been
  updated from 0.7.0 to 0.8.0. The new release supports
  XEP-0085: Chat State Notifications and XEP-0313: Message
  Archive Management.
- [Poezio][poezio], a console-based XMPP client as been updated from 0.13.1
  to 0.14. Poezio is now under GPLv3+. The new release supports
  request for voice and the /join command support using an XMPP
  URI. More information at
  https://lab.louiz.org/poezio/poezio/-/raw/v0.14/CHANGELOG.
- [Swift][swift-im], back in Debian is the Swift XMPP client - a
  cross-platform Client written in C++. In 2015 the client was
  removed from testing and is back with version 5.0.

## Server

- [prosody] the lightweight extensible XMPP server has been
  upgraded from 0.11.9 to 0.12.2. Mobile and connectivity
  optimizations, a new module for HTTP file sharing, audio/video
  calling support. See the [release
  announcement](https://blog.prosody.im/prosody-0.12.2-released/)
  for more info. You will also find a lot of new modules which have
  been added to [0.12.0](https://blog.prosody.im/prosody-0.12.0-released/).
  The version 0.12.3 is waiting migration from unstable to testing.
- [ejabberd], extensible realtime platform (XMPP server + MQTT
  broker + SIP service) has been updated from Version 21.01 to 23.01.
  The new version supports the latest version of MIX (XEP-0369).
  There were also changes for SQL and MUC. See the [release
  information for 22.10](https://www.process-one.net/blog/ejabberd-22-10/)
  and [23.01](https://www.process-one.net/blog/ejabberd-23-01/) for more details.

## Libs

- [libstrophe], xmpp C lib has been upgraded from 0.10.1 to
  0.12.2. The lib has SASL EXTERNAL support (XEP-0178),
  support for manual certificate verification and Stream Management
  support (XEP-0198).
- [python-nbxmpp] 2.0.2 to 4.2.0 - used by gajim
- [qxmpp] 1.3.2 to 1.4.0
- [slixmpp] 1.7.0 to 1.8.3 (see
  https://lab.louiz.org/poezio/slixmpp/-/tags/slix-1.8.0)
- [loudmouth] 1.5.3 to 1.5.4
- [libomemo-c], new in Debian with version 0.5.0 - a fork of libsignal-protocol-c

## Others

- There were some changes of the Libervia, formerly known as Salut à Toi (SaT) packages in Debian.
  The most visible change is, that Salut à Toi has been renamed to libervia:
  - salutatoi is now libervia-backend (0.9.0)
  - sat-xmpp-primitivus is now libervia-tui
  - sat-xmpp-core is now libervia-backend
  - sat-xmpp-jp is now libervia-cli
  - sat-pubsub is now libervia-pubsub (0.4.0)
- gsasl has been updated from 1.10.0 to 2.2.0
- libxeddsa 2.0.0 is new in Debian - toolkit around Curve25519 and Ed25519 key pairs

Happy chatting - keep in touch with your family and friends via Jabber / XMPP -
XMPP is an open standard of the Internet Engineering Task Force (IETF) for instant messaging.

[dino-im]:https://packages.debian.org/bookworm/dino-im
[ejabberd]:https://packages.debian.org/bookworm/ejabberd
[gajim]:https://packages.debian.org/bookworm/gajim
[kaidan]:https://packages.debian.org/bookworm/kaidan
[poezio]:https://packages.debian.org/bookworm/poezio
[profanity]:https://packages.debian.org/bookworm/profanity
[prosody]:https://packages.debian.org/bookworm/prosody
[prosody-modules]:https://packages.debian.org/bookworm/prosody-modules
[libstrophe]:https://packages.debian.org/bookworm/libstrophe-dev
[libomemo-c]:https://packages.debian.org/bookworm/libomemo-c-dev
[loudmouth]:https://packages.debian.org/bookworm/libloudmouth1-dev
[qxmpp]:https://packages.debian.org/bookworm/libqxmpp-dev
[slixmpp]:https://packages.debian.org/bookworm/python3-slixmpp
[python-nbxmpp]:https://packages.debian.org/bookworm/python3-nbxmpp
